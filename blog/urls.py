from django.urls import path
from . import views

urlpatterns = [
    path('', views.list_posts, name='list_posts'),
    path('create/', views.create_post_view, name='create_post'),        # C
    path('<slug>/', views.detail_post_view, name='detail_post'),        # R
    path('edit/<slug>', views.edit_post_view, name='edit_post'),        # U
    path('delete/<slug>', views.delete_post_view, name='delete_post'),  # D
]
