from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout, update_session_auth_hash
from .forms import RegistrationForm, LoginForm, ProfileUpdateForm
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.decorators import login_required
from django.contrib import messages


def registration_view(request):
    context = {
        'username': request.user,
    }
    if request.POST:
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save(commit=False)
            email = form.cleaned_data.get('email')
            password = form.cleaned_data.get('password1')
            form.save()
            account = authenticate(email=email, password=password)
            login(request, account)
            return redirect('index')
        else:
            context['registration_form'] = form
    else:
        if request.user.is_authenticated:
            return redirect('index')
        else:
            form = RegistrationForm()
            context['registration_form'] = form
    return render(request, 'account/register.html', context)


def logout_view(request):
    logout(request)
    return redirect('index')


def login_view(request):
    context = {}
    if request.user.is_authenticated:
        return redirect('index')
    else:
        if request.POST:
            form = LoginForm(request.POST)
            if form.is_valid():
                login_behaviour(request)
                return redirect('index')
        else:
            form = LoginForm()
        context['login_form'] = form
        return render(request, 'account/login.html', context)


def login_behaviour(request):
    email = request.POST['email']
    password = request.POST['password']
    auth = authenticate(email=email, password=password)
    if auth:
        login(request, auth)


@login_required
def profile_view(request):
    context = {}
    if request.POST:
        form = ProfileUpdateForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
    else:
        form = ProfileUpdateForm(initial={
            'email': request.user.email,
        })
    context['profile_form'] = form
    return render(request, 'account/profile.html', context)


@login_required
def change_password_view(request):
    if request.POST:
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            messages.success(request, 'Your password has been changed.')
            return redirect('change_password')
    else:
        form = PasswordChangeForm(request.user)
    context = {
        'change_password_form': form
    }
    return render(request, 'account/change_password.html', context)