from django.shortcuts import render
from account.models import Account


def index_view(request):
    context = {
        'accounts': Account.objects.all(),
        'username': request.user,
    }
    return render(request, 'index.html', context)

