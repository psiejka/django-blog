from django.urls import path
from . import views

app_name = 'blog'

urlpatterns = [
    path('<slug>/', views.post_detail, name='post_detail'),
    path('<slug>/update', views.post_update, name='post_update'),
    path('<slug>/delete', views.post_delete, name='post_delete'),
    path('create', views.create_post, name='create_post'),
    path('list', views.PostListView.as_view(), name='list_posts'),
]