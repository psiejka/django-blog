# Blog site implemented with Python 3.7 and Django 2.2

## Features
* Customized Admin panel for created models
* Custom account and account manager 
* Ability to upload media files
* Forms for login/registration
* ~~Poor~~ Bootstrap usage
* CRUD for Posts
* Search bar
* Pagination
* REST API for blog posts with token authentication

## Installing
Clone this repository

* use Docker

```console
docker-compose up
```
		
* use virtual environment

```console
python3 -m venv venv
```

```console
source venv/Scripts/activate
```

```console
pip install -r requirements.txt
```

```console
python manage.py makemigrations
```

```console
python manage.py migrate
```

```console
python manage.py runserver
```

## Resources
##### Django App:
* Antonio Mele - Django 2 by Example, Packt Publishing (2018)
* [Django Documentation](https://docs.djangoproject.com/en/2.2/)
##### REST API:
* [REST API Guide](https://www.django-rest-framework.org/)
* [REST API Tutorial](https://codingwithmitch.com/courses/build-a-rest-api)
