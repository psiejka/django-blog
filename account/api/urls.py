from django.urls import path
from rest_framework.authtoken import views

app_name = 'blog'


urlpatterns = [
    #  https://www.django-rest-framework.org/api-guide/authentication/#generating-tokens
    path('login', views.obtain_auth_token),
]