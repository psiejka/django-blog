from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate
from account.models import Account


class RegistrationForm(UserCreationForm):
    email = forms.EmailField(max_length=80, help_text='Your email adress.')

    class Meta:
        model = Account
        fields = ('email', 'username', 'password1', 'password2')


class LoginForm(forms.ModelForm):
    password = forms.CharField(label='Password', widget=forms.PasswordInput)

    class Meta:
        model = Account
        fields = ('email', 'password')

    def clean(self):
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')
        if not authenticate(email=email, password=password):
            raise forms.ValidationError('Invalid credentials')


class ProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = Account
        fields = ('email', )

    def clean_email(self):
        email = self.cleaned_data['email']
        try:
            account = Account.objects.all().exclude(pk=self.instance.pk).get(email=email)
        except Account.DoesNotExist:
            return email
        raise forms.ValidationError(f'There is {email} in database. :(')

