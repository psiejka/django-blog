from rest_framework import serializers
from blog.models import Post


class PostSerializer(serializers.ModelSerializer):
    author_username = serializers.SerializerMethodField('get_author')
    author_email = serializers.SerializerMethodField('get_author_email')

    class Meta:
        model = Post
        fields = ['author_username', 'author_email', 'title', 'body', 'image', 'date_published', 'date_updated', 'slug']

    def get_author(self, post):
        author_username = post.author.username
        return author_username

    def get_author_email(self, post):
        author_email = post.author.email
        return author_email
