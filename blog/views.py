from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.paginator import Paginator
from django.db.models import Q
from blog.models import Post
from blog.forms import CreatePostForm
from account.models import Account


def list_posts(request):
    posts = Post.objects.all().order_by('-date_published')

    query = request.GET.get("q")
    if query:
        posts = posts.filter(
            Q(title__icontains=query) | Q(body__icontains=query) | Q(author__username__icontains=query)
        ).distinct().order_by('-date_published')

    paginator = Paginator(posts, 2)
    page = request.GET.get('page')
    posts = paginator.get_page(page)
    context = {
        'posts': posts,
    }
    return render(request, 'blog/list_posts.html', context)


def detail_post_view(request, slug):
    post = get_object_or_404(Post, slug=slug)
    return render(request, 'blog/detail_post.html', {'post': post})


def edit_post_view(request, slug):
    post = get_object_or_404(Post, slug=slug)
    if request.user != post.author:
        return redirect('index')
    form = CreatePostForm(data=request.POST or None, files=request.FILES or None, instance=post)
    if form.is_valid():
        form.save()
        messages.success(request, 'Post has been updated successfully.')
        return redirect('detail_post', slug=slug)
    return render(request, 'blog/create_post.html', {'create_post_form': form,
                  'post': post})


def delete_post_view(request, slug):
    post = get_object_or_404(Post, slug=slug)
    if request.user != post.author:
        return redirect('index')
    if request.POST:
        post.delete()
        return redirect('list_posts')
    return render(request, 'blog/confirm_delete.html', {'post': post})


@login_required
def create_post_view(request):
    context = {}
    if request.POST:
        form = CreatePostForm(data=request.POST, files=request.FILES)
        if form.is_valid():
            form = form.save(commit=False)
            author = Account.objects.filter(email=request.user.email).first()
            form.author = author
            form.save()
            context['create_post_form'] = form
            messages.success(request, 'Post has been created successfully.')
            return redirect('detail_post', slug=form.slug)
        else:
            context['create_post_form'] = form
    else:
        form = CreatePostForm()
        context['create_post_form'] = form
    return render(request, 'blog/create_post.html', context)