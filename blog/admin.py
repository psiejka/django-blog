from django.contrib import admin
from blog.models import Post


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ['title', 'author', 'date_published']
    readonly_fields = ['date_published', 'date_updated', 'slug']
    search_fields = ['title', 'author']