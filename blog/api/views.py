from blog.api.serializers import PostSerializer
from blog.models import Post

from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication
from rest_framework import generics
from rest_framework.pagination import PageNumberPagination


@api_view(['GET', ])
def post_detail(request, slug):
    try:
        post = Post.objects.all().get(slug=slug)
    except Post.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    serializer = PostSerializer(post)
    return Response(serializer.data)


@permission_classes(IsAuthenticated)
@api_view(['PUT', ])
def post_update(request, slug):
    try:
        post = Post.objects.all().get(slug=slug)
    except Post.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if post.author != request.user:
        return Response({'response': 'You don\'t have permission to do this operation.'})

    serializer = PostSerializer(post, data=request.data, partial=True)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@permission_classes(IsAuthenticated)
@api_view(['DELETE', ])
def post_delete(request, slug):
    try:
        post = Post.objects.all().get(slug=slug)
    except Post.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if post.author != request.user:
        return Response({'response': 'You don\'t have permission to do this operation.'})

    post.delete()
    return Response(status=status.HTTP_204_NO_CONTENT)


@permission_classes(IsAuthenticated)
@api_view(['POST', ])
def create_post(request):
    post = Post()
    post.author = request.user

    serializer = PostSerializer(post, data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PostListView(generics.ListAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    authentication_classes = (TokenAuthentication, )
    permission_classes = (IsAuthenticated, )
    pagination_class = PageNumberPagination
