from django.contrib import admin
from account.models import Account
# Register your models here.


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = ['email', 'username', 'is_admin']
    readonly_fields = ['date_joined', 'last_login']
    search_fields = ['email', 'username']